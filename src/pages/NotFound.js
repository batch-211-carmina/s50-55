import Banner from '../components/Banner.js';

export default function NotFound() {

	const data = {
		title: "404 - Page Not Found",
		content: "The requested URL was not found on this server.",
		destination: "/",
		label: "Back to Home",
	}

	return (
		// "prop" name is up to the developer
		<Banner bannerProp = {data}/>
	)
}