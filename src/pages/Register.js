// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';


// S54 ACTIVITY - Conditional Rendering if user is loggin-in, /register should not accessible
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext'
import { Navigate } from 'react-router-dom';

/*
  Mini Activity - add input fields for firstName, lastName, and mobileNo
*/

// Define state hooks for all inut fields and an "isActive" state for conditional rendering of the submit btn

export default function Register() {

  // Allows us to consume the User Context/Data and its properties for validation
  const { user } = useContext(UserContext);

  // create state hooks to store the values of the input field
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  //create a state to determine whether the submit button is enabled or not

  const [isActive, setIsActive] = useState('');

  console.log(firstName);
  console.log(lastName);
  console.log(mobileNo);
  console.log(email);
  console.log(password1);
  console.log(password2);

  function registerUser(e) {

    e.preventDefault();

    setFirstName("");
    setLastName("");
    setMobileNo("");
    setEmail("");
    setPassword1("");
    setPassword2("");

    alert("Thank you for registring!");
  }

  /*
    Two Way Binding
    - It is done so that we can assure that we can save the input into our states as we type into the input elements. This is done so what we don't have to save it just before we submit

    e.target - current element where the event happens
    e.target.value - current value of the element where the event happened
  */

  useEffect(() => {

    if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && password1 === password2 ){
      setIsActive(true);
    }
    else {
      setIsActive(false);
    }

  }, [firstName, lastName, mobileNo, email, password1, password2]);

  return (
    // S54 Activity continuation of solution code
    // Conditional Rendering
    // LOGIC - if there is a user logged-in in the web application, endpoint or "/register" should not be accessible. The user should be navigated to courses tab instead.

    (user.id !== null)
    ?
    <Navigate to="/courses" />
    // ELSE - if the localStorage is empty, the user is allowed to access the login page.
    :
    <div className="my-3">
      <h2>Register</h2>
      <Form onSubmit={(e) => registerUser(e)}>
         <Form.Group className="mt-3 mb-3" controlId="firstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control
          type="text"
          placeholder="Enter First Name"
          value={firstName}
          onChange={e => setFirstName(e.target.value)}
          required/>
        </Form.Group>

         <Form.Group className="mb-3" controlId="lastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
          type="text"
          placeholder="Enter Last Name"
          value={lastName}
          onChange={e => setLastName(e.target.value)}
          required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="mobileNo">
          <Form.Label>Mobile Number</Form.Label>
          <Form.Control
          type="text"
          placeholder="+639-xxx-xxxx"
          value={mobileNo}
          onChange={e => setMobileNo(e.target.value)}
          required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          required/>
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={e => setPassword1(e.target.value)}
          required/>
        </Form.Group>

         <Form.Group className="mb-3" controlId="password2">
          <Form.Label>Password</Form.Label>
          <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={e => setPassword2(e.target.value)}
          required/>
        </Form.Group>

        {isActive ?
        <Button variant="success" type="submit" id="submitBtn">
          Submit
        </Button>
        :
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
        }

      </Form>
    </div>
  );
}
